window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();
window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();

function closest(el, selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}

window.addEventListener('DOMContentLoaded', function() {
  if (window.jQuery) window.Velocity = window.jQuery.fn.velocity;
  new ResponsiveWatcher();
  /*new ExtraNav();*/
  new MenuSp();
  new Sticky();
  if (document.querySelector('.bx_arcoding')) {
    new Qa();
  }
});

var ResponsiveWatcher = (function() {
  function ResponsiveWatcher() {
    var _this = this;
    this.responsiveRatio = 769;
    this.isSP = window.innerWidth >= this.responsiveRatio ? true : false;
    this.isFirstTrigger = true;
    this.domRespnosiveImage = document.getElementsByClassName('resimg');
    this.init = function() {
      window.addEventListener("resize", _this.OnResize, false);
    };
    this.OnResize = function() {
      var i = 0 | 0;
      if (window.innerWidth >= _this.responsiveRatio && _this.isSP) {
        _this.isSP = false;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].setAttribute("src", _this.domRespnosiveImage[i].getAttribute("src").replace("_sp.", "_pc."));
        };
      }
      if (window.innerWidth < _this.responsiveRatio && !_this.isSP) {
        _this.isSP = true;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].setAttribute("src", _this.domRespnosiveImage[i].getAttribute("src").replace("_pc.", "_sp."));
        };
      }
      if (_this.isFirstTrigger) {
        _this.isFirstTrigger = false;
        for (i = 0; i < _this.domRespnosiveImage.length; i = (i + 1) | 0) {
          _this.domRespnosiveImage[i].style.visibility = "visible";
        }
      }
    };
    window.addEventListener('load', this.init);
    this.OnResize();
  }
  return ResponsiveWatcher;
}())

/*var ExtraNav = function () {
  function ExtraNav() {
    var e = this;
    this.clone = document.getElementById('login').cloneNode(true);
    this.clone_2 = document.getElementById('search').cloneNode(true);
    this.clone.removeAttribute('id');
    this.clone_2.removeAttribute('id');
    this.des = document.getElementById('bx_menu');
    this.des.appendChild(this.clone);
    this.des.appendChild(this.clone_2);
  }
  return ExtraNav;
}();*/

var MenuSp = (function(){
  function MenuSp(){
    var m = this;
    this._target = document.getElementById('icon_menu');
    this._mobile = document.getElementById('bx_menu');
    this._target.addEventListener('click',function(){
      if(this.classList.contains('open')){
        this.classList.remove('open');
        m._mobile.classList.remove('open');
        m._mobile.style.height = 0;
        document.body.style.overflow = 'inherit';
      } else {
        this.classList.add('open');
        m._mobile.classList.add('open');
        document.body.style.overflow = 'hidden';
        m._mobile.style.height = window.innerHeight-closest(m._target,'header').clientHeight+'px';
      }
    })
    this._reset = function(){
      if(m._target.classList.contains('open')){
        if(window.innerWidth > 769) {
          m._target.classList.remove('open');
          m._mobile.classList.remove('open');
          document.body.removeAttribute('style');
          m._mobile.style.height = 'auto';
        } else {
          m._mobile.style.height = window.innerHeight-closest(m._target,'header').clientHeight+'px';
        }
      } else {
        if(window.innerWidth < 769) {
          m._mobile.style.height = 0;
        } else {
          m._mobile.style.height = 'auto';
        }
      }
    }
    m._reset();
    window.addEventListener('resize',m._reset,false);
  }
  return MenuSp;
})()

var Sticky = (function(){
  function Sticky(){
    var s = this;
    this._target = document.getElementById('header');
    this._mobile = document.getElementById('bx_menu');
    this._for_sp = function(top){
      s._mobile.style.top = s._target.clientHeight+'px';
      if(top > 0) {
        document.body.style.paddingTop = s._target.clientHeight+'px';
        s._target.classList.add('fixed');
      } else {
        document.body.style.paddingTop = 0;
        s._target.classList.remove('fixed');
      }
    }
    this.handling = function(){
      var _top  = document.documentElement.scrollTop || document.body.scrollTop;
      var _left  = document.documentElement.scrollLeft || document.body.scrollLeft;
      if(window.innerWidth < 769) {
        s._for_sp(_top);
      } else {
        s._for_sp(_top);
      }
    }
    window.addEventListener('scroll',s.handling,false);
    window.addEventListener('resize',s.handling,false);
    window.addEventListener('load',s.handling,false);
    this.handling();
  }
  return Sticky;
})()

var Qa = (function(){
  function Qa(){
    var q = this;
    this.wraps = document.querySelector('.bx_arcoding');
    this.first = true;
    this.handling = function(k){
      Array.prototype.forEach.call(q.wraps.querySelectorAll('dt'),function(question,l){
        if(l == k) {
          if(question.classList.contains('active')){
            question.classList.remove('active');
            question.nextElementSibling.style.height = 0;
          } else {
            question.classList.add('active');
            question.nextElementSibling.style.height =  question.nextElementSibling.querySelector('.list_item').clientHeight+2+'px';
          }
        }
      })
    }
    this.hand_resize = function() {
      Array.prototype.forEach.call(q.wraps.querySelectorAll('dt'),function(question,i){
        if(question.classList.contains('active')){
          question.nextElementSibling.style.height =  question.nextElementSibling.querySelector('.list_item').clientHeight+2+'px';
        }
      })
    }
    Array.prototype.forEach.call(this.wraps.querySelectorAll('dt'),function(question,i){
      question.addEventListener('click',function(){
        q.handling(i);
      })
    })
    q.handling(0);
    window.addEventListener('resize',q.hand_resize,false);
  }
  return Qa;
})()