<div class="row">
  <div class="bx_logo">
    <h1 class="logo"><a href="/"><span>リボンサテライト</span>Ribbon Satellite</a></h1>
    <p>豊富なカラーから選べるオリジナルリボン</p>
  </div>
  <!--/.bx_logo-->
  <div class="right_icon">
    <div class="bx_cart">
      <a href="../shopping_cart/">
        <span><img src="../../common/images/icon_cart.png" alt="Cart"></span>
        <em>CART</em>
      </a>
    </div>
    <!--/.bx_cart-->
    <div id="icon_menu" class="icon_nav">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <em>MENU</em>
    </div>
  </div>
  <!--/.right_icon-->
  <div class="bx_menu" id="bx_menu">
    <section class="st_login" id="login">
      <h2 class="ttl_st">ログイン</h2>
      <div class="bx_login">
        <form class="frm_login" method="post">
          <div class="frm_group">
            <label><i class="icon_mail"></i>Email</label>
            <div class="ip_right">
              <input type="email" class="frm_control" name="Email">
            </div>
            <!--/.ip_right-->
          </div>
          <!--frm_group-->
          <div class="frm_group">
            <label><i class="icon_lock"></i>パスワード</label>
            <div class="ip_right">
              <input type="password" class="frm_control" name="Password">
            </div>
            <!--/.ip_right-->
          </div>
          <!--frm_group-->
          <div class="frm_btn">
            <button class="btn btn_submit" type="submit">ログイン</button>
            <a class="btn btn_reg" href="#">新規会員登録はこちらへ </a>
          </div>
          <!--/.frm_btn-->
        </form>
        <!--/.frm_login-->
        <p>※パスワードをお忘れの方は<a href="#">こちらへ</a></p>
      </div>
      <!--/.bx_login-->
    </section>
    <!--/.st_login-->
    <section class="st_search" id="search">
    <h2 class="ttl_st">商品検索</h2>
    <form class="frm_search">
      <div class="frm_group">
        <div class="ip_left">
          <input class="frm_control" type="text" name="Search">
        </div>
        <button class="btn_search" type="submit">検索</button>
      </div>
      <!--/.frm_group-->
    </form>
    <!--/.frm_search-->
    <em>※キーワードからサイト内の商品を検索します。</em>
  </section>
  <!--/.st_search-->
  </div>
  <!--/.bx_menu-->
</div>
<!--/.row-->