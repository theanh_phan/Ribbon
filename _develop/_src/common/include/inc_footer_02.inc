<div class="row">
  <div class="bx_arcoding" id="arcoding">
    <dl>
      <dt>リボン エクスプレス<span>(Ribbon Express)>>(80)</span></dt>
      <dd>
        <div class="list_item">
          <ul>
            <li><a href="#">ポリエステル 幅：13 mm(40)</a></li>
            <li><a href="#">ポリエステル 幅：24 mm(40)</a></li>
          </ul>
        </div>
        <!--/.list_item-->
      </dd>
    </dl>
    <dl>
      <dt>リボン クラシック<span>(Ribbon Classic)>>(178)</span></dt>
      <dd>
        <div class="list_item">
          <ul>
            <li><a href="#">ポリエステル 幅：13 mm(40)</a></li>
            <li><a href="#">ポリエステル 幅：24 mm(40)</a></li>
          </ul>
        </div>
        <!--/.list_item-->
      </dd>
    </dl>
    <dl>
      <dt>Art Label<span>(1)</span></dt>
      <dd>
        <div class="list_item">
          <ul>
            <li><a href="#">ポリエステル 幅：13 mm(40)</a></li>
            <li><a href="#">ポリエステル 幅：24 mm(40)</a></li>
          </ul>
        </div>
        <!--/.list_item-->
      </dd>
    </dl>
  </div>
  <!--/.bx_arcoding-->
  <section class="st_news">
    <h2 class="ttl_st">NEWS/お知らせ</h2>
    <ul class="list_news">
      <li>
        <a href="#">
          <time datetime="2007-07-03">2007-07-03</time>
          <em>ご利用方法はこちらからご覧下さい。</em>
        </a>
      </li>
    </ul>
    <!--/.list_news-->
  </section>
  <!--st_news-->
  <section class="st_popular">
    <h2 class="ttl_st">人気商品BEST3</h2>
    <ul class="list_popular">
      <li>
        <a href="#">
          <figure>
            <span><img src="../../common/images/img_pop_01.png" alt="images popular 01"></span>
            <figcaption>ポリエステル(エクスプレス) リボン色：No.001（白色） 幅：13 mm</figcaption>
          </figure>
        </a>
      </li>
      <li>
        <a href="#">
          <figure>
            <span><img src="../../common/images/img_pop_02.png" alt="images popular 02"></span>
            <figcaption>ポリエステル(エクスプレス) リボン色：No.033（黒色） 幅：13 mm</figcaption>
          </figure>
        </a>
      </li>
      <li>
        <a href="#">
          <figure>
            <span><img src="../../common/images/img_pop_01.png" alt="images popular 01"></span>
            <figcaption>ポリエステル(エクスプレス) リボン色：No.001（白色） 幅：24 mm</figcaption>
          </figure>
        </a>
      </li>
    </ul>
    <!--/.list_popular-->
  </section>
  <!--/.st_popular-->
</div>
<!--/.row-->