<div class="row">
  <section class="st_login">
    <h2 class="ttl_st">ログイン</h2>
    <div class="bx_login">
      <form class="frm_login" method="post">
        <div class="frm_group">
          <label><i class="icon_mail"></i>Email</label>
          <div class="ip_right">
            <input type="email" class="frm_control" name="Email">
          </div>
          <!--/.ip_right-->
        </div>
        <!--frm_group-->
        <div class="frm_group">
          <label><i class="icon_lock"></i>パスワード</label>
          <div class="ip_right">
            <input type="password" class="frm_control" name="Password">
          </div>
          <!--/.ip_right-->
        </div>
        <!--frm_group-->
        <div class="frm_btn">
          <button class="btn btn_submit" type="submit">ログイン</button>
          <a class="btn btn_reg" href="#">新規会員登録はこちらへ </a>
        </div>
        <!--/.frm_btn-->
      </form>
      <!--/.frm_login-->
      <p>※パスワードをお忘れの方は<a href="#">こちらへ</a></p>
    </div>
    <!--/.bx_login-->
  </section>
  <!--/.st_login-->
  <section class="st_shopping_cart">
    <h2 class="ttl_st">ショッピングカート</h2>
    <div class="gr_cart">
      <div class="gr_item_pro">
        <div class="item_pro">
          <p class="name">商品名商品名商品名</p>
          <!--/.name-->
          <p class="number">×1</p>
          <!--/.number-->
          <p class="monney">000
            <ins>円</ins>
          </p>
          <!--/.monney-->
        </div>
        <!--/.item_pro-->
        <div class="item_pro">
          <p class="name">商品名商品名商品名</p>
          <!--/.name-->
          <p class="number">×1</p>
          <!--/.number-->
          <p class="monney">000
            <ins>円</ins>
          </p>
          <!--/.monney-->
        </div>
        <!--/.item_pro-->
        <div class="item_pro">
          <p class="name">商品名商品名商品名</p>
          <!--/.name-->
          <p class="number">×1</p>
          <!--/.number-->
          <p class="monney">000
            <ins>円</ins>
          </p>
          <!--/.monney-->
        </div>
        <!--/.item_pro-->
      </div>
      <!--/.gr_item_pro-->
      <div class="total_c">
        <p>000<strong>円</strong></p>
      </div>
      <!--/.total_c-->
    </div>
    <!--/.gr_cart-->
  </section>
  <!--/.st_shopping_cart-->
  <section class="st_search">
    <h2 class="ttl_st">商品検索</h2>
    <form class="frm_search">
      <div class="frm_group">
        <div class="ip_left">
          <input class="frm_control" type="text" name="Search">
        </div>
        <button class="btn_search" type="submit">検索</button>
      </div>
      <!--/.frm_group-->
    </form>
    <!--/.frm_search-->
    <em>※キーワードからサイト内の商品を検索します。</em>
  </section>
  <!--/.st_search-->
</div>